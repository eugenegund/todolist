import session_model as sess_model

from werkzeug.wrappers import Request
from flask import redirect, make_response

class SessionMW(object):
    def __init__(self, app):
        self.app = app
        self.valid_sess = False
    def __call__(self, environ, start_response):#*args, **kwargs):
        self.id_sess = environ['werkzeug.request'].cookies.get('id_session')
        self.id_user = sess_model.user_id(self.id_sess)

        return self.app(environ, start_response)