import sys
sys.path.append("../web/item/")

from dbcm import Connection
from datetime import datetime, timedelta
import uuid

dbconfig = {'host': '127.0.0.1',
                'user': 'postgres',
                'password': '11111111',
                'database': 'session_list',}


def add(id_user:str) -> str:
    """  Add session fot user. Return id session"""
    if not bool(id_user):
        return '';

    id_session = str(uuid.uuid4())#generate_uuid()

    time_now = datetime.now()

    with Connection(dbconfig) as cursor:
        sql = 'insert into public."sessions" (id_user, id_session, time) values(%s, %s, %s)'
        cursor.execute(sql, (id_user, id_session, str(time_now.timestamp())))

    return id_session


def remove_session(id_session:str):
    """ Remove the session for id """
    with Connection(dbconfig) as cursor:
        cursor.execute('delete from public."sessions" where id_session = %s ', (id_session, ))


def remove_user(id_user:str):
    """ Remove the session for id """
    with Connection(dbconfig) as cursor:
        cursor.execute('delete from public."sessions" where id_user = %s ', (id_user, ))


def valid_sess(id_session:str) -> bool:
    """  Check session fot user"""
    ans = []
    time_now = datetime.now()

    with Connection(dbconfig) as cursor:
        sql = 'select * from public."sessions"  where id_session=%s '
        cursor.execute(sql, (id_session, ))
        if not bool(cursor.rowcount):
            return False
        ans = cursor.fetchall()

    if time_now.timestamp() - float(ans[0][-1]) >= 600: #86400
        remove_session(id_session)
        return False

    return True


def user_id(id_session:str) -> str:
    """  Return id user for session"""
    time_now = datetime.now()

    ans = []
    with Connection(dbconfig) as cursor:
        sql = 'select * from public."sessions"  where id_session=%s '
        cursor.execute(sql, (id_session, ))
        if not bool(cursor.rowcount):
            return ''
        ans = cursor.fetchall()

    if not valid_sess(id_session):
        return ''

    return ans[0][0]


def sessions(id_user:str) -> list:
    """  Return list all session for user"""
    ans = []
    with Connection(dbconfig) as cursor:
        sql = 'select * from public."sessions"  where id_user=%s '
        cursor.execute(sql, (id_user,))
        if not bool(cursor.rowcount):
            return ans
        ans = list(cursor)

    return ans


#Читерская фича
def show_all() -> list:
    ans = []
    with Connection(dbconfig) as cursor:
        sql = 'select * from public."sessions" '
        cursor.execute(sql, ())
        if not bool(cursor.rowcount):
            return ans
        
        ans = list(cursor)

    return ans


def remove_all_session():
    ans = show_all()

    with Connection(dbconfig) as cursor:
        for sess in ans:
            sql = 'delete from public."sessions" where id_session = %s '
            cursor.execute(sql, (sess[1],))