import items_crypto
import sys
sys.path.append("../item/")

if __name__ == '__main__':
    new_pass = input('Введите пароль: ')
    hashed_password = crypto.to_hash(new_pass)
    print('Строка для хранения в базе данных: ' + hashed_password)
    old_pass = input('Введите пароль еще раз для проверки: ')

    if crypto.check_password(hashed_password, old_pass):
        print('Вы ввели правильный пароль')
    else:
        print('Извините, но пароли не совпадают')