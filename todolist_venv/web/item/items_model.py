import uuid

from dbcm import Connection


dbconfig = {'host': '127.0.0.1',
                'user': 'postgres',
                'password': '11111111',
                'database': 'elems_list',}


def description(table:str) -> list:
    """ Return  description the table"""
    descr = []
    with Connection(dbconfig)  as cursor:
        sql = 'select * from public."%s" %s' % (table, 'offset %s limit %s')
        cursor.execute(sql, (0,1))
        descr = [descr[0].lower() for descr in cursor.description]

    return descr


def count_items(id_user:str) -> int:
    """ Return count elements the db """
    item_number = 0
    with Connection(dbconfig) as cursor:
        sql = 'select * from public."elements"  where id_users = %s '
        cursor.execute(sql, (id_user,))
        item_number = cursor.rowcount

    return item_number


def count_users() -> int:
    """ Return count users the db """
    item_number = 0
    with Connection(dbconfig) as cursor:
        sql = 'select * from public."users" '
        cursor.execute(sql, ())
        item_number = cursor.rowcount

    return item_number


def add_item(el:dict, id_user:str):
    """ Add new item to base """
    if not (bool(el['title'] and bool(el['text'])) or not bool(id_user)):
        return

    id = el['id']
    if not bool(el['id']):
        id = str(uuid.uuid4())#generate_uuid('elements')

    with Connection(dbconfig) as cursor:
        sql = 'insert into public."elements" (id, title, text, id_users) values(%s, %s, %s, %s)'
        cursor.execute(sql, (id, el['title'], el['text'], id_user))


def add_user(login:str, password:str, id:str='') -> str:
    """  Add new user to base"""
    if not bool(login) or not bool(password):
        return ''

    if not bool(id):
        id = str(uuid.uuid4())#generate_uuid('users')

    with Connection(dbconfig) as cursor:
        sql = 'select * from public."users" where login=%s'
        cursor.execute(sql, (login,))
        if bool(cursor.rowcount):
            return 'Login exists'
        sql = 'insert into public."users" (id, login, password) values(%s, %s, %s)'
        cursor.execute(sql, (id, login, password))

    return id


def password_user(login:str) -> str:
    """ Return password user """
    password = ''
    if bool(login):
        with Connection(dbconfig) as cursor:
            sql = 'select * from public."users" where login=%s'
            cursor.execute(sql, (login,))
            if not bool(cursor.rowcount):
                return password
            password = cursor.fetchall()[0][-1]

    return password


def user_id(login:str) -> str:
    """  Return id the user"""
    id = ''
    with Connection(dbconfig) as cursor:
        sql = 'select * from public."users" where login=%s'
        cursor.execute(sql, (login,))
        if not bool(cursor.rowcount):
            return id
        id = cursor.fetchall()[0][0]

    return id


def items(id_user:str, count:int, offset:int) -> list:
    """ Return the count elements(List the dict).
    Start item number to find - offset
    """
    return_elements = []
    if count == 0 or offset < 0:
        return return_elements

    rows = []
    with Connection(dbconfig) as cursor:
        sql = 'select public."elements".* from  public."elements" INNER JOIN  public."users" ON ' \
              '(public."elements"."id_users" = public."users"."id") where id_users=%s offset %s limit %s'
        cursor.execute(sql, (id_user, offset, count))
        rows = list(cursor)

    if not bool(rows):
        return return_elements

    one_element = dict()
    field_names = description('elements')
    if not bool(rows):
        return return_elements

    for row in rows:
        for one_line in list(zip(field_names, row)):
            one_element.setdefault(one_line[0], one_line[1])
        return_elements.append(one_element.copy())
        one_element.clear()

    return return_elements


def remove_item(id_user:str, uuid_elements:list):
    """ Remove the elements for uuid """
    if 'list' not in str(type(uuid_elements)):
        return

    for uuid in uuid_elements:
        answ = []
        with Connection(dbconfig) as cursor:
            cursor.execute('delete from public."elements" where id = %s and id_users = %s', (uuid, id_user))


def remove_user(id_user:str):
    """  Remove user. Return True - OK, False - error"""
    remove_all_items_user(id_user)
    with Connection(dbconfig) as cursor:
        cursor.execute('delete from public."users" where id = %s', (id_user,))


def remove_all_items_user(id_user:str):
    """ Remove the all elements user"""
    with Connection(dbconfig) as cursor:
        cursor.execute('delete from public."elements" where id_users = %s', (id_user,))


# Читерские методы
def all_users() -> list:
    """ Return list all users """
    ans = []

    with Connection(dbconfig) as cursor:
        sql = 'select * from public."users" '
        cursor.execute(sql, ())
        ans = list(cursor)

    return ans


def all_items() -> list:
    """ Return list all items """
    ans = []

    with Connection(dbconfig) as cursor:
        sql = 'select * from public."elements" '
        cursor.execute(sql, ())
        ans = list(cursor)

    return ans


def remove_all_users():
    """ Remove the all users """
    users = []
    with Connection(dbconfig) as cursor:
        sql = 'select * from public."users" '
        cursor.execute(sql, ())
        users = list(cursor)

    with Connection(dbconfig) as cursor:
        for user in users:
            remove_all_items_user(user[0])
            cursor.execute('delete from public."users" where id = %s', (user[0],))


def remove_all_items():
    """ Remove the all elements """
    with Connection(dbconfig) as cursor:
        cursor.execute('delete from public."elements"', ())