import psycopg2 as sql_lib

class Connection:
    def __init__(self, config:dict) -> None:
        self.configuration = config
    def __enter__(self) -> 'cursor':
        self.connect = sql_lib.connect(**self.configuration)
        self.cursor = self.connect.cursor()
        return self.cursor
    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        self.connect.commit()
        self.cursor.close()
        self.connect.close()