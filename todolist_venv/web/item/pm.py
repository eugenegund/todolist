import math

class PageIterator:
    def __init__(self, count_items:int, current_page:int=0, count_items_page:int=5):
        self.count_items = count_items
        self.count_items_page = count_items_page
        self.count_page = max(1 ,math.ceil(self.count_items / self.count_items_page))
        self.current_page = current_page % self.count_page
        self.offset = self.current_page * self.count_items_page
    def prev(self) -> int:
        return (self.current_page - 1 + self.count_page) % (self.count_page)
    def next(self) -> int:
        return  (self.current_page + 1) % self.count_page
