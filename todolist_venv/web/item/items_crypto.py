import uuid
import hashlib


def to_hash(word:str) -> str:
    """  Hashing a word. Return hashed word"""
    salt = uuid.uuid4().hex
    return hashlib.sha256(salt.encode() + word.encode()).hexdigest() + '|' + salt


def check_password(hashed_word:str, user_word:str) -> bool:
    """ Сhecking the hash. Return True - passed, False - not pass """
    if not bool(hashed_word) or not bool(user_word):
        return False

    word, salt = hashed_word.split('|')
    return word == hashlib.sha256(salt.encode() + user_word.encode()).hexdigest()
