from functools import wraps
from flask import g, redirect, request, render_template, make_response

import items_crypto as crypto
import items_model
import session_model as sess_model


def auth_required(func:object):
    """ Decorator for verifying user authentication """
    @wraps(func)
    def wrapper(*args, **kwargs):
        if  g.id_user:
            return func(*args, **kwargs)

        return redirect('/login')

    return wrapper


def registration() -> '302':
    """ New user registration """
    if request.method == 'GET':
        return render_template('registration.html', title='Registration')
    else:
        if bool(request.form['login']) and bool(request.form['password']):
            password = crypto.to_hash(request.form['password'])
            id_user = items_model.add_user(request.form['login'], password)

        return redirect('/login')


def login(id_session) -> '302':
    """ User login  """
    if request.method == 'GET':
        return render_template('login.html', title='Login')
    else:
        password = items_model.password_user(request.form['login'])
        if crypto.check_password(password, request.form['password']):
            id_user = items_model.user_id(request.form['login'])

            res = make_response(redirect('/'))
            if not sess_model.valid_sess(id_session):
                res.set_cookie('id_session', sess_model.add(id_user), max_age=600)

            return res

    return redirect('/login')


def logout(id_session) -> '302':
    """ User logout """
    sess_model.remove_session(id_session)

    res = make_response(redirect('/'))
    res.set_cookie('id_session', '0', max_age = 0)

    return res