import math
from flask import Flask, render_template, request, redirect, make_response, g
import sys
sys.path.append("../web/item/")

import items_model
import session_model as sess_model
from  pm import PageIterator
import items_crypto as crypto
from  middleware import SessionMW
from authentication import auth_required
import authentication as auth_user


app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z]/'
app.wsgi_app = SessionMW(app.wsgi_app)


@app.before_request
def before_request():
    g.id_user = app.wsgi_app.id_user


@app.route('/move')
@auth_required
def move() -> '302':
    current_page = int(request.args.get('current_page', 0))
    return redirect('/?current_page='+str(current_page))


@app.route('/remove',  methods=['POST', 'GET'])
@auth_required
def remove() -> '302':
    id_session = request.cookies.get('id_session', '')
    id_user = sess_model.user_id(id_session)

    pageiterator = PageIterator(items_model.count_items(id_user), int(request.args.get('current_page', 0)))
    if request.method == 'GET':
        elements = items_model.items(id_user, pageiterator.count_items_page, pageiterator.offset)
        return render_template('remove.html',
                               the_title='Delete items',
                               user = 'id: %s' % (id_user),
                               list=elements,
                               p=pageiterator)
    else:
        items_model.remove_item(id_user, list(request.form.keys()))
        return redirect('/?current_page=' + str(pageiterator.current_page))

    return redirect('/')



@app.route('/add', methods=['POST', 'GET'])
@auth_required
def add() -> '302':
    if request.method == 'GET':
        return render_template('add.html',
                               the_title = 'Add items',
                               user = 'id: %s' % (g.id_user),
                               current_page = request.args.get('current_page', 0))
    else:
        items_model.add_item({'id':'', 'title':request.form['title'], 'text':request.form['text']}, g.id_user)
        return redirect('/?current_page=' + str(request.args.get('current_page', 0)))

    return redirect('/')



@app.route('/')
@auth_required
def items() -> 'html':

    pageiterator = PageIterator(items_model.count_items(g.id_user), int(request.args.get('current_page', 0)))
    elemens = items_model.items(g.id_user, pageiterator.count_items_page, pageiterator.offset)
    return render_template('main.html',
                           title = 'List of elements with %s entries ' % (pageiterator.count_items_page),
                           user = 'id: %s' % (g.id_user),
                           p = pageiterator,
                           list = elemens)


@app.route('/registration', methods=['POST', 'GET'])
def reg() -> '302':
    return auth_user.registration()


@app.route('/login', methods=['POST', 'GET'])
def login() -> '302':
    return auth_user.login(app.wsgi_app.id_sess)


@app.route('/logout')
@auth_required
def logout() -> '302':
    return auth_user.logout(app.wsgi_app.id_sess)


if __name__ == '__main__':
    app.run(debug=True)